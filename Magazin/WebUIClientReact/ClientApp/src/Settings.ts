﻿export class Settings {
    public static readonly ProductURL = "/api/Balance/GetProductBalance";
    public static readonly NewsUrl = "/api/News";
    public static readonly CatsUrl = "/api/Category/GetAll";


    public static readonly RegisterUrl = "/api/account/register";
    public static readonly LoginUrl = "/api/account/login";

    public static readonly OrderUrl ="/api/Order";
}