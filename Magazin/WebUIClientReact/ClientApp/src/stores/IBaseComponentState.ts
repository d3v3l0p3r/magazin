﻿
export interface IBaseComponentState {
    readonly isBusy: boolean;
    readonly errorMessage: string;
}