﻿namespace WebApi.Options
{
    /// <summary>
    /// Основные настройки
    /// </summary>
    public class MainOptions
    {
        /// <summary>
        /// Использовать интеграцию с 1С
        /// </summary>
        public bool UseOneAssIntegration { get; set; }
    }
}
