﻿using BaseCore.Security.Entities;

namespace BaseCore.Security.Services.Abstract
{
    public interface IUserRepository
    {
        IUserRepository Create();
    }

  
}