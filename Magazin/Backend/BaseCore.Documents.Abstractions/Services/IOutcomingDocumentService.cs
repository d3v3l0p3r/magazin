﻿using BaseCore.DAL.Implementations.Entities.Documents;
using BaseCore.Documents.Implementations.Services.Abstractions;

namespace BaseCore.Documents.Abstractions.Services
{
    public interface IOutcomingDocumentService : IBaseDocumentService<OutComingDocument, OutComingDocumentEntry>
    {

    }
}
