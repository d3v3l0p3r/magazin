﻿namespace IdentityServiceBase.Services.Abstract
{
    public interface IUserRepository
    {
        IUserRepository Create();
    }
}